# Powi DENIS, Sys Admin

### Table des matières / Table of contents

<!--[TOC]-->
* [FR :fr:](#fr-)
    * [Projets personnels & collectifs](#projets-personnels-collectifs)
    * [Bénévolats](#bénévolats)
    * [Emplois et Stages](#emplois-et-stages)
    * [Diplômes](#diplômes)
    * [Compétences](#compétences)
    * [Passions & Loisirs](#passions-loisirs)
* [EN :us:](#en-)
    * [Personnal and collectives projects](#personnal-and-collective-projects)
    * [Volunteering](#volunteering)
    * [Employment and Intership](#employment-and-intership)
    * [Degrees](#degrees)
    * [Skills](#skills)
    * [Passions & Hobbies](#passions-hobbies)

## FR :fr: :

### Projets personnels & collectifs

* [Juin 18 → \*] QueerTube, projet colletif de plateforme PeerTube à destination des personnes queers (en réponse à la censure de celles-ci par Youtube).

    * Community Management
    * Création d’une communauté autour du projet
    * Prises de décision en collectif
    
* [Décembre 17 → \*] Bot Discord Loup Garou, projet personnel de bot Discord pour jouer aux *![Loups Garous de Thiercellieux](https://fr.wikipedia.org/wiki/Les_Loups-garous_de_Thiercelieux)* :

    * Utilisation de python avec objets et API.
    * Utilisation de git.
    * Le bot gère les rôles Discord et les rôles du jeu, distribue les rôles aux joueurses, gère les personnes éliminées, l’accès restreint aux salons,…
    
#### Bénévolats

* 2017-18 [La Ressourcerie Verte](http://laressourcerieverte.com/), Conseillère Collégiale : Prises de décisions en collectif, rédactions de comptes rendus, facilitations de débats.
* 2016 Framasoft : Projet Framinetest – Mise en place, accompagnement des équipes pédagogiques, configurations, administration système.
* 2015-16 Association Chalonnaise pour une Transition Energétique [ACTE] – Écriture de comptes rendus, actions type collectes de soutien, mémorial (Tchernobyl, Fukushima).

#### Emplois et Stages

* Octobre 2016 → Juin 2017 : Service Civique, **La Ressourcerie Verte** (4-5 salarié·e·s, ~20 bénévoles)

    * Installation de GNU/Linux sur les machines
    * Migration de Dropbox à NextCloud
    * Initiation à Dolibarr
    * Formations des utilisateurices aux nouveaux outils

* Juin → Décembre 2015 : Technicienne Informatique, **CAD** (4 salarié·e·s)

    * Monter un Wordpress
    * Récupérer des données et images de manière automatisée (javascript) depuis le site du fournisseur
    * Améliorer le design d’un site
    * Maintenance d’ordinateurs et de réseaux

### Diplômes

* 2014 · BTS IRIS (Informatique et Réseau pour l’Industrie et les Services techniques)
* 2012 · Bac STI (Sciences Techniques Industrielles), spécialité Électrotechique

### Compétences

* Langages de Programmation, Scripting, Syntaxe
    * [✦✦✦✦✩] Python
    * [✦✦✦✦✩] Markdown
    * [✦✦✦✩✩] HTML/CSS
    * [✦✦✦✩✩] C/C++
    * [✦✦✦✩✩] bash
    * [✦✦✦✩✩] SQL
    * [✦✦✦✩✩] git
    * [✦✦✩✩✩] php
    * [✦✦✩✩✩] javascript (&jquery)

* Administration Système (serveur)
    * [✦✦✦✦✩] GNU/Linux [Debian / CentOS]
        * [✦✦✦✦✩] Nginx
        * [✦✦✦✦✩] Gestion de configurations
        * [✦✦✦✩✩] Postfix & Dovecot
        * [✦✦✦✩✩] Apache
        * [✦✦✦✩✩] LetsEncrypt
    * [✦✩✩✩✩] BSD

* Social
    * [✦✦✦✦✩] Prises de décisions en collectif
    * [✦✦✦✦✩] Rédactions de comptes rendus de réunions
    * [✦✦✦✦✩] Facilitation de débats
    * [✦✦✦✩✩] Gestion de communauté

### Passions & Loisirs

* [❤️❤️❤️❤️❤️] Progertion en python
* [❤️❤️❤️❤️❤️] Administrer mon serveur debian
* [❤️❤️❤️❤️🖤] Parler en public (conférence, débats,…)
* [❤️❤️❤️❤️🖤] Vélo
* [❤️❤️❤️🖤🖤] Scripting bash

## EN :us: :

### Personal and collective projects

* [June 18 → \*] QueerTube, a collective PeerTube platform project for queer people (in response to Youtube censorship of those ones).

    * Community Management
    * Creating a community around that project
    * Collective decisions making
    
* [December 17 → \*] Loup Garou Discord Bot, personnal project on creating a Discord bot to play *![The Werewolves of Millers Hollow](https://en.wikipedia.org/wiki/The_Werewolves_of_Millers_Hollow)* :

    * Use of python with objects and APIs
    * Use of git.
    * The bot manages Discord and game roles, distributes roles to players, manages eliminated players, restricts access to channels,…
    
#### Volunteering

* 2017-18 ![La Ressourcerie Verte](http://laressourcerieverte.com/) (= *The Green Ressourcerie*), member of the community council : Collective decisions making, writing reports, debates facilitation.
* 2016 Framasoft : Framinetest Project – Setting up, accompaniment of educational team, configurations, system administration
* 2015-16 Association Chalonnaise pour une Transition Energétique (= *Chalonnaise Organization for an Energy Transition*) [ACTE] – Writing reports, actions like support collects, memorials (Chernobyl, Fukushima)

#### Employment and Intership

* October 2016 → June 2017 : Civic Service, **La Ressourcerie Verte** (4-5 salaries, ~20 volunteers)

    * GNU/Linux installations
    * Migration from Dropbox to NextCloud
    * Introduction to Dolibarr
    * Formations of users on new tools

* June → December 2015 : IT Technician, **CAD** (4 salaries)

    * Set up a Wordpress
    * Automaticaly (JavaScript) get datas and images from a provider website
    * Improve a website design
    * Maintenance of computers and networks

### Degrees

* 2014 · BTS IRIS (IT & Networks for Industry and Technic Services)
* 2012 · Bac STI (Industrial Technic Sciences), Electrotechnic

### Skills

* Programmation & Scripting & Syntax Languages
    * [✦✦✦✦✩] Python
    * [✦✦✦✦✩] Markdown
    * [✦✦✦✩✩] HTML/CSS
    * [✦✦✦✩✩] C/C++
    * [✦✦✦✩✩] bash
    * [✦✦✦✩✩] SQL
    * [✦✦✦✩✩] git
    * [✦✦✩✩✩] php
    * [✦✦✩✩✩] javascript (&jquery)

* System Administration (server)
    * [✦✦✦✦✩] GNU/Linux [Debian / CentOS]
        * [✦✦✦✦✩] Nginx
        * [✦✦✦✦✩] Configurations management
        * [✦✦✦✩✩] Postfix & Dovecot
        * [✦✦✦✩✩] Apache
        * [✦✦✦✩✩] LetsEncrypt
    * [✦✩✩✩✩] BSD

* Social
    * [✦✦✦✦✩] Collective decision making
    * [✦✦✦✦✩] Writing reunions reports
    * [✦✦✦✦✩] Debates facilitation
    * [✦✦✦✩✩] Community managing

### Passions & Hobbies

* [❤️❤️❤️❤️❤️] Python programming
* [❤️❤️❤️❤️❤️] Administrate my debian server
* [❤️❤️❤️❤️🖤] Public talk (conferences, debates,…)
* [❤️❤️❤️❤️🖤] Cycling
* [❤️❤️❤️🖤🖤] Bash scripting
